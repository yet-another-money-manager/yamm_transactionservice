## YAMM Transaction Service

__This code is under heavy development, and it's not ready for production yet, only for local development.__

This repository contains the code for the YAMM Service responsible for all the operations with expenses, incomes and transfers of a customer.

### Authentication Pattern

In the MVP, there isn't any kind of authentication used by the BFF (part of YAMM Client) to access the services endpoints.

### Business Rules

This service contains all the rules and logic related to the basic functionalities involving expenses, incomes and transfers registered by the customers.

---

## Dependencies

To run the project in your local dev enviroment, you will need:

* [RVM](https://rvm.io/)
* [Ruby 2.5.1](https://www.ruby-lang.org/pt/)
* [Rails 5.2.1](http://rubyonrails.org/)
* [Bundler](http://bundler.io/)
* [Postgresql](https://www.postgresql.org/)
* [Docker & Docker-Compose](https://www.docker.com/)

All the instructions to install these tools can be found on file [TOOLS_INSTALL](/TOOLS_INSTALL.md).

---

## Running the YAMM Transaction Service

After dependencies installation, clone the repository and execute the following commands:

```bash
$ git clone git@bitbucket.org:yet-another-money-manager/yamm_transactionservice.git
$ cd yamm_transactionservice/
$ docker-compose up -d
$ bin/setup
```

If you want to run _psql_ in the running container, execute the following command:

```bash
$ docker run -it --rm --link yamm-postgres:postgres --net transactionservice_postgres_network postgres psql -h postgres -U transaction_service
```

---

## Tests, Code Quality & Style

This project relies on some tools to ensure the appropriate test coverage, besides the code quality and cleanessny.

It's important to stick with this rules, to maintain an high code quality and the app bugs-free

### Tests

Tests creation and execution follow [RSpec patterns](http://rspec.info/). To run them, execute the following command:

```bash
$ bundle exec rspec
```

### Coverage

After each Test Suite execution, an report showing the overall coverage is created by SimpleCov gem. This report can be found at the folder **coverage/**. Just open the file **rcov/index.html** on your browser.

### Code Style

The code style checks are made with the rules configured on [Rubocop](http://batsov.com/rubocop/), just run the command:

```bash
$ rubocop
```

If some offense is found, its possible to execute the command `rubocop -a` to make an auto-correction of some errors/warnings. If it still have some problems, they will need to be corrected manually.

### Code Quality

The general code quality verification is made by the rules of [Rubycritic](https://github.com/whitesmith/rubycritic/) gem. To generate an assessment, just run the command below:

```bash
$ rubycritic app lib
```

### Code Documentation

To view all the code documentation, run the following command:

```bash
$ yard server
```

Go to `http://localhost:8808/` address to browse the generated docs.

\*\* If you don't have Yard installed, execute the command `gem install yard`

---

## Credits

@danilo_barion

---

## Contributions

1. Clone the repo!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Send your branch to the repository: `git push origin my-new-feature`
5. Open an Pull Request :D
