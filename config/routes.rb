# frozen_string_literal: true

Rails.application.routes.draw do
  get '/healthcheck' => 'healthcheck#index'

  api_version(module: 'V1', path: { value: 'v1', "defaults=format": 'json' }) do
    resources :users do
      resources :accounts do
        resources :expenses
        resources :incomes
      end

      resources :categories
    end

    # Rotas necessárias
    # {root}/users
    # => GET    obtém usuários
    # => POST   cria usuário
    # => PATCH  atualiza usuário
    # => DELETE remove usuário, todas suas contas, categorias, despesas e receitas relacionadas

    # {root}/users/{user_id}/accounts
    # => GET    obtém contas do usuário
    # => POST   cria conta do usuário
    # => PATCH  atualiza conta do usuário
    # => DELETE remove uma conta do usuário e todas suas despesas/receitas relacionadas

    # {root}/users/{user_id}/categories
    # => GET    obtém categorias do usuário
    # => POST   cria categoria do usuário
    # => PATCH  atualiza categoria do usuário
    # => DELETE remove uma categoria do usuário e todas suas despesas/receitas relacionadas

    # {root}/users/{user_id}/accounts/{account_id}/expenses
    # => GET    obtém despesas de uma conta específica do usuário
    # => POST   cria despesa de uma conta específica do usuário
    # => PATCH  atualiza despesa de uma conta específica do usuário
    # => DELETE remove uma despesa de uma conta específica do usuário

    # {root}/users/{user_id}/accounts/{account_id}/incomes
    # => GET    obtém receitas de uma conta específica do usuário
    # => POST   cria receita de uma conta específica do usuário
    # => PATCH  atualiza receita de uma conta específica do usuário
    # => DELETE remove uma receita de uma conta específica do usuário
  end
end
