# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

effected = TransactionStatus.create(name: 'effected',
                                    description: 'Expenses already effected on the account',
                                    status: 1)
non_effected = TransactionStatus.create(name: 'non_effected',
                                        description: 'Expenses that are not effected yet',
                                        status: 1)

class DatabaseData
  def initialize(effected, non_effected)
    @effected = effected
    @non_effected = non_effected
  end

  def danilo_data
    danilo_id = User.create(name: 'Danilo', email: 'danilo@gmail.com').id

    foods = Category.create(user_id: danilo_id,
                            name: 'Foods',
                            description: 'Expenses related to food consuming',
                            category_type: 'expenses')
    car = Category.create(user_id: danilo_id,
                          name: 'Car',
                          description: 'Expenses related to car manutention and accessories',
                          category_type: 'expenses')
    investments = Category.create(user_id: danilo_id,
                                  name: 'Investments',
                                  description: 'Incomes associated with investments',
                                  category_type: 'incomes')

    bradesco = Account.create(user_id: danilo_id,
                              name: 'Bradesco',
                              description: '',
                              initial_balance: 0.0,
                              current_balance: 999.0)
    cef = Account.create(user_id: danilo_id,
                         name: 'CEF',
                         description: '',
                         initial_balance: 50.0,
                         current_balance: -10.0)

    car_expense = Expense.create(name: 'Fixing Ford Ka',
                                 amount: 10.0,
                                 date: Date.today,
                                 transaction_status_id: @effected.id,
                                 account_id: bradesco.id,
                                 category_id: car.id)
    tesouro_income = Income.create(name: 'Tesouro Direto 2020',
                                   amount: 1000.0,
                                   date: Date.today + 1,
                                   transaction_status_id: @non_effected.id,
                                   account_id: cef.id,
                                   category_id: investments.id)
  end

  def alessandra_data
    alessandra_id = User.create(name: 'Alessandra', email: 'ale@gmail.com').id

    health = Category.create(user_id: alessandra_id,
                             name: 'Health',
                             description: 'Expenses related to health care',
                             category_type: 'expenses')
    salary = Category.create(user_id: alessandra_id,
                             name: 'Salary',
                             description: 'Incomes associated with salary',
                             category_type: 'incomes')

    itau = Account.create(user_id: alessandra_id,
                          name: 'Itaú',
                          description: '',
                          initial_balance: 0.0,
                          current_balance: 444.0)

    health_expense = Expense.create(name: 'Hair cut',
                                    amount: 35.90,
                                    date: Date.today,
                                    transaction_status_id: @effected.id,
                                    account_id: itau.id,
                                    category_id: health.id)
    salary_income = Income.create(name: 'August salary',
                                  amount: 3000.0,
                                  date: Date.today + 1,
                                  transaction_status_id: @non_effected.id,
                                  account_id: itau.id,
                                  category_id: salary.id)
  end
end

dd = DatabaseData.new(effected, non_effected)
dd.danilo_data
dd.alessandra_data

rosana_id = User.create(name: 'Rosana', email: 'rosana@gmail.com').id
pedro_id = User.create(name: 'Pedro', email: 'pedro@gmail.com').id
