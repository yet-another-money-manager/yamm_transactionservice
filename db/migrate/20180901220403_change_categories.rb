# frozen_string_literal: true

class ChangeCategories < ActiveRecord::Migration[5.2]
  def change
    change_table :categories do |t|
      t.rename :type, :category_type
    end
  end
end
