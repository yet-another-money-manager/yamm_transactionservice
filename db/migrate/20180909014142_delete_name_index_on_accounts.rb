class DeleteNameIndexOnAccounts < ActiveRecord::Migration[5.2]
  def change
    remove_index :accounts, :name
    add_index(:accounts, [:user_id, :name], unique: true)
  end
end
