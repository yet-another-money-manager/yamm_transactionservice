# frozen_string_literal: true

class CreateAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :accounts do |t|
      t.string :name, null: false
      t.string :description
      t.decimal :initial_balance, null: false
      t.decimal :current_balance, null: false

      t.timestamps
    end
    add_index :accounts, :name, unique: true
  end
end
