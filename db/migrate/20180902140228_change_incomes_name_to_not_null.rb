class ChangeIncomesNameToNotNull < ActiveRecord::Migration[5.2]
  def change
    change_column :incomes, :name, :string, null: false
  end
end
