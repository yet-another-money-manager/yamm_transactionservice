class ChangeUserIdToNotNullOnCategories < ActiveRecord::Migration[5.2]
  def change
    change_column :categories, :user_id, :integer, null: false
  end
end
