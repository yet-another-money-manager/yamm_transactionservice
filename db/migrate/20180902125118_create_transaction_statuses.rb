# frozen_string_literal: true

class CreateTransactionStatuses < ActiveRecord::Migration[5.2]
  def change
    create_table :transaction_statuses do |t|
      t.string :name, null: false, limit: 20
      t.string :description, limit: 60
      t.boolean :status, null: false, default: true

      t.timestamps
    end
    add_index :transaction_statuses, :name
  end
end
