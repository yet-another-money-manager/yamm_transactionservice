# frozen_string_literal: true

class CreateCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :categories do |t|
      t.string :name, null: false, limit: 20
      t.string :description, limit: 60
      t.string :type, null: false, limit: 10

      t.timestamps
    end
    add_index :categories, :name
    add_index :categories, :type
  end
end
