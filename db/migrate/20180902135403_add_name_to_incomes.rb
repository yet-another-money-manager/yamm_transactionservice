class AddNameToIncomes < ActiveRecord::Migration[5.2]
  def change
    add_column :incomes, :name, :string
  end
end
