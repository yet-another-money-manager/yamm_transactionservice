class ChangeExpenseNameToNotNull < ActiveRecord::Migration[5.2]
  def change
    change_column :expenses, :name, :string, null: false
  end
end
