class AddStatusToAccounts < ActiveRecord::Migration[5.2]
  def change
    add_column :accounts, :status, :boolean, default: true
  end
end
