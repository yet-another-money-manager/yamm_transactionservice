class ChangeUserIdToNotNullOnAccount < ActiveRecord::Migration[5.2]
  def change
    change_column :accounts, :user_id, :integer, null: false
  end
end
