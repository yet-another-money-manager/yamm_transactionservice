# frozen_string_literal: true

class CreateIncomes < ActiveRecord::Migration[5.2]
  def change
    create_table :incomes do |t|
      t.decimal :amount
      t.date :date
      t.references :transaction_status, foreign_key: true
      t.references :account, foreign_key: true
      t.references :category, foreign_key: true

      t.timestamps
    end
  end
end
