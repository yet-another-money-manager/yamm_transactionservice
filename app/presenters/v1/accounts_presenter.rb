# frozen_string_literal: true

class V1::AccountsPresenter < V1::BasePresenter
  def initialize(accounts)
    @accounts = accounts
  end

  def as_json(options = {})
    # fill me in...
  end

  def to_xml(options = {})
    xml = options[:builder] ||= Builder::XmlMarkup.new
    # fill me in...
  end
end
