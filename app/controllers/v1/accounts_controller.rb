# frozen_string_literal: true

class V1::AccountsController < V1::BaseController
  INDEX = Dry::Validation.Params do
    required(:user_id).filled(:int?)
    optional(:status).filled(:bool?)
  end
  def index
    result = INDEX.call(params.to_unsafe_h)
    return render json: { error: result.errors } unless result.success?

    valid_params = result.output

    accounts = Account.where(user_id: valid_params[:user_id])
    accounts = accounts.where(status: valid_params[:status]) unless valid_params[:status].nil?

    render json: { accounts: accounts }
  end

  CREATE = Dry::Validation.Params do
    required(:user_id).filled(:int?)
    required(:name).filled(:str?)
    optional(:description).maybe(:str?)
    required(:initial_balance).filled(:decimal?)
    required(:current_balance).filled(:decimal?)
  end
  def create
    result = CREATE.call(params.to_unsafe_h)
    return render json: { error: result.errors } unless result.success?

    # Ajustar o índice para considerar user_id e nome da Conta e não somente nome da conta
    account = Account.create(result.output)

    render json: account
  end
end
