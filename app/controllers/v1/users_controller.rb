# frozen_string_literal: true

class V1::UsersController < V1::BaseController
  def index
    render json: { users: User.all }
  end
end
