# frozen_string_literal: true

class V1::CategoriesController < V1::BaseController
  INDEX = Dry::Validation.Params do
    required(:user_id).filled(:int?, gt?: 0)
  end
  def index
    result = INDEX.call(params.to_unsafe_h)
    return render json: { error: result.errors } unless result.success?

    render json: { categories: Category.where(user_id: result.output[:user_id]) }
  end

  CREATE = Dry::Validation.Params do
    required(:user_id).filled(:int?)
    required(:name).filled(:str?)
    optional(:description).maybe(:str?)
    required(:category_type).filled(included_in?: %w[expenses incomes])
  end
  def create
    result = CREATE.call(params.to_unsafe_h)
    return render json: { error: result.errors } unless result.success?

    category = Category.create(result.output)

    render json: category
  end

  UPDATE = Dry::Validation.Params do
    required(:id).filled(:int?)
    required(:user_id).filled(:int?)
    optional(:name).filled(:str?)
    optional(:description).maybe(:str?)
    optional(:category_type).filled(included_in?: %w[expenses incomes])
  end
  def update
    result = UPDATE.call(params.to_unsafe_h)
    return render json: { error: result.errors } unless result.success?
    parameters = result.output

    category = Category.where(id: parameters[:id], user_id: parameters[:user_id])
    return render json: { error: :not_found }, status: 404 if category.empty?

    category.update(parameters)

    render json: category
  end
end
