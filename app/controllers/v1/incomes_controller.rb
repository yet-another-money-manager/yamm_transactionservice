# frozen_string_literal: true

class V1::IncomesController < V1::BaseController
  def index
    render json: { incomes: Income.all }
  end

  CREATE = Dry::Validation.Params do
    required(:name).filled(:str?)
    optional(:amount).filled(:decimal?)
    required(:date).filled(:date?)
    required(:status_id).filled(included_in?: TransactionStatus.all.map { |status| status.id })
    required(:category_id).filled(:int?)
    required(:account_id).filled(:int?)
  end
  def create
    result = CREATE.call(params.to_unsafe_h)
    return render json: { error: result.errors } unless result.success?

    incomes = Income.create(result.output)

    render json: incomes
  end
end
