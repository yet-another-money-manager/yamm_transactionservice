# frozen_string_literal: true

# Healthcheck endpoint for the service
class HealthcheckController < ApplicationController
  def index
    render json: {
      service_version: 'v1',
      status: :ok,
      dependencies: {
        services: [],
        databases: check_database
      }
    }, status: 200
  end

  private

  def check_database
    res = ActiveRecord::Base.connection_pool.with_connection { |conn| conn.exec_query("SELECT 'ok';") }
    { status: res ? :ok : :not_ok }
  end
end
