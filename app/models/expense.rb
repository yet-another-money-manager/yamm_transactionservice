# frozen_string_literal: true

class Expense < ApplicationRecord
  belongs_to :transaction_status
  belongs_to :account
  belongs_to :category
end
